(function() {

    //settings api
    const settings = {
        "url": "/api/catalog_system/pub/category/tree/3/",
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
    };

    //contruÃ§Ã£o do menu
    $.ajax(settings).done(function(response) {
            let max = 0;

            //menu mobile
            if (large < 992) {

                for (response of response) {

                    loadMenu(response)

                }

                initMobile();
                $('.main-menu').prepend("<li class='menu-item'><a href='/account'><p>Login | Cadastro </p></a></li>")
            }

            //menu desktop
            if (large >= 992) {

                for (response of response) {
                    if (max < 7) {
                        loadInFooter(response)
                    }
                    if (max <= 3) {
                        loadMenu(response)
                    } else {
                        loadMoreMenu(response)
                    }
                    max = max + 1;
                }

            }


        })
        //Link element
    function loadInFooter(response) {
        const footerElement = document.querySelector('.js-categorias-footer');
        const liElement = document.createElement('li');
        const linkElement = document.createElement('a');
        linkElement.setAttribute('href', response.url);
        linkElement.textContent = response.name;
        liElement.appendChild(linkElement)
        footerElement.appendChild(liElement)
    }

    //LOAD MENU
    function loadMenu(menu) {
        const menuElement = document.querySelector('.main-menu');
        let child = menu.children;
        const ulElement = document.createElement('ul');
        const liElement = document.createElement('li');
        const linkElement = document.createElement('a');
        const pElement = document.createElement('p');
        const maxElement = document.createElement('div');
        linkElement.setAttribute('href', menu.url);
        pElement.textContent = menu.name;
        liElement.setAttribute('class', 'menu-item');
        linkElement.setAttribute('class', 'link-main-menu')
        maxElement.setAttribute('class', 'sub-menu');
        linkElement.appendChild(pElement);
        liElement.appendChild(linkElement);
        menuElement.appendChild(liElement);
        maxElement.appendChild(ulElement)

        for (child of child) {
            const subLiElement = document.createElement('li');
            const subLinkElement = document.createElement('a');

            subLiElement.setAttribute('class', 'sub-menu-item');
            subLinkElement.setAttribute('href', child.url);
            ulElement.setAttribute('class', 'max-container');
            subLinkElement.textContent = child.name;

            subLiElement.appendChild(subLinkElement);
            ulElement.appendChild(subLiElement);
            liElement.appendChild(maxElement);
        }
    }

    //LOAD MORE MENU
    function loadMoreMenu(menu) {
        const menuElement = document.querySelector('.all-menu .max-container');

        let child = menu.children;
        const liElement = document.createElement('li');
        const linkElement = document.createElement('a');
        const subUlElement = document.createElement('ul')

        liElement.setAttribute('class', 'all-item');
        linkElement.setAttribute('href', menu.url);

        linkElement.textContent = menu.name;
        liElement.appendChild(linkElement);
        menuElement.appendChild(liElement);

        for (child of child) {
            const subLiElement = document.createElement('li');
            const subLinkElement = document.createElement('a');

            subLiElement.setAttribute('class', 'sub-menu-all-item');
            subLinkElement.setAttribute('href', child.url);
            subUlElement.setAttribute('class', 'sub-menu-all-item-container');
            subLinkElement.textContent = child.name;

            subLiElement.appendChild(subLinkElement);
            subUlElement.appendChild(subLiElement);
            liElement.appendChild(subUlElement);
        }
    }

})();

console.log(`
================================
||    Desenvolvido por:       ||
||    AgÃªncia Vinci           ||  
================================
`)




//largura da minha tela
const large = window.innerWidth

//CARRINHO
$('.toggler-cart').click(function(e) {
    e.preventDefault()
    $('.carrinho').slideToggle();
})





function initMobile() {


    $('.container-all-itens,.sub-menu').before("<span class='open-sub'></span>")

    $('.main-head > .max-container').prepend(`
        <span class="toggle-menu">
            <span class="line line-1"></span>
            <span class="line line-2"></span>
            <span class="line line-3"></span>
        </span>
   `);

    $('.toggle-menu').click(function() {
        $(this).toggleClass('active');
        $('.menu-container').toggleClass('active');
        $('.box-shadow').fadeToggle();
        $('body').toggleClass('menu-open')
    })

    $('.open-sub').click(function() {
        $(this).siblings('.sub-menu,.container-all-itens').slideToggle();
    })

    $('.btn-buscar').click(function() {
        $('.container-search .busca').toggleClass('active');
        $('.container-search').toggleClass('active');
    })

}

setTimeout(() => {
    $('.ui-autocomplete').css({
        "max-width": $('.search-container .busca').width()
    })
}, 1000);
