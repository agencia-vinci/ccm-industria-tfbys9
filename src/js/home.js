$('.helperComplement').remove()
    //BANNER
const $fullbanner = $(".main-banner > article");
if ($fullbanner.length) {
    $fullbanner.owlCarousel({
        items: 1,
        loop: true,
        singleItem: true,
        autoPlay: true,
        stopOnHover: true,
        navigation: true,
        autoHeight: true,
        navigationText: ['<img src="/arquivos/seta-banner-esquerda.png" class="arrow-banner">', '<img src="/arquivos/seta-banner-direita.png" class="arrow-banner">'],
    });
}

//THIS FUNCTIONS LOAD OWL IN ALL VITRINES IN HOME PAGE AND DO VALIDATIONS
$('.js-vitrine').each(function(vitrine) {
    //VALIDATION TO CHECK IF EXISTS ITEMS ARE IN VITRINE
    const child = $(this).find('.vitrine ul')

    if (!child) {
        $(child).remove()
        return
    }
    //take text to title
    $(this).find('.title-vitrine p').text($(this).find('.vitrine h2').text())
    $(this).find('.vitrine h2').remove()

    //start vitrine
    $(this).find('.vitrine').owlCarousel({
        items: 4,
        autoPlay: true,
        autoHeight: true,
        pagination: false,
        itemsDesktop: [1200, 4],
        itemsDesktopSmall: [991, 3],
        itemsTablet: [767, 2],
        itemsMobile: [567, 1],
        navigation: true,
        stopOnHover: true,
        autoHeight: true,
        navigationText: ['<img src="/arquivos/seta-banner-esquerda.png" class="img-responsive arrow-vitrine" />', '<img src="/arquivos/seta-banner-direita.png" class="img-responsive arrow-vitrine" />'],
    });
})