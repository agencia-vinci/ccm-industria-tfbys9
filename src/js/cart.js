const cartElement = document.querySelector('.carrinho')
const cart = document.querySelector('.calc-frete .btn-frete')
cart.addEventListener('click', calcShipping);


/* 
  muda o formato de apresentacao da vtex (500) para dinheiro (5,00)
*/
function toMoney(e) {
    let text = e
    text = parseInt(text)
    text = text.toFixed(2)
    text = text * 0.010

    text = text.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
    return text
}

/*
  como as alteracoes dos item sao feitar pelo id do item no array de orderform
  e necessario manter os item organizados

  esta funcao organiza os itens e inicia as funcoes passando os parametros com 
  a ordem que esta no orderform, sempre q um item e removido essa funcao e chamada
  para orgarnizar tudo novamente, para cada funcao (adicionar, remover e deletar)
*/
function orderItens() {
    let i = 0
    let j = 0
    let k = 0
    document.querySelectorAll('.btn-delete').forEach(function(target) {
        target.setAttribute('onclick', 'deleteItem(this,' + i + ')')
        i++;
    })
    document.querySelectorAll('.btn-add').forEach(function(target) {
        target.setAttribute('onclick', 'addItem(this,' + j + ')')
        j++;
    })
    document.querySelectorAll('.btn-remove').forEach(function(target) {
        target.setAttribute('onclick', 'removeItem(this,' + k + ')')
        k++;
    })
    cartElement.setAttribute('class', 'carrinho');
}

/* 
  esta funcao monta os itens do carrinho
  ela pode receber qualquer parametro desejado que esteja no orderform
  voce pode definir a estrutura html que quiser na variavel element
  e adicionar os parametros usando a sintaxe de "template literals" => ${variavel}

  essa funcao e chamada no inicio da montagem do carrinho para carregar os itens
*/
function renderItemTemplate(target, title, imageUrl, skuName, refCode, brand, itemQtd, itemPrice, itemUrl) {

    let element = `
  <div class="cart-item">
    <a href="${itemUrl}" class="cart-img">
        <img src="${imageUrl.replace('-55-55/','-250-250/')}"/>
    </a>

    <div class='cart-details'>
        <a href="${itemUrl}+" class='item-name'>${title}</a>
        <span class='item-sku-name'>${skuName}</span>
        <span class='item-ref-code'>cod: ${refCode}</span>
        <span class='item-brand'>${brand}</span>
    </div>
    <div class='box-valor'>
        <div class='box-qtd'>
            <span class='box-title'>Quantidade</span>
            <div class='btn-remove btn-action'>-</div>
            <span class="item-qtd">${itemQtd}</span>
            <div class='btn-add btn-action'>+</div>
        </div>

        <span class='item-price'>
          <span class='box-title'>Valor</span>
          <strong>${itemPrice}</strong>
        </span>

        <button class='btn-delete btn-action'>X</button>
    </div>
  </div>
  `

    target.innerHTML += element
}

/* 
  esta funcao atualiza o total do carrinho
  e necessario chamala e passar o orderform toda vez que alguma coisa e alterada
  se a sua estrutura de total for diferente 
  voce deve mudar as classes que estao sendo chamadas para manipular o DOM
*/
function loadingTotal(orderForm) {
    let totalizers = orderForm.totalizers;
    let total = 0
    let discount = 0
    let frete = 0
    for (totalizers of totalizers) {
        if (totalizers.id === 'Shipping') {
            frete = totalizers.value
        }
        if (totalizers.id === 'Discount') {
            discount = totalizers.value
        }
        if (totalizers.id === 'Items') {
            total = totalizers.value
        }
    }
    document.querySelector('.box-subtotal .money').textContent = toMoney(total)
    document.querySelector('.box-discounts .money').textContent = toMoney(discount)
    document.querySelector('.box-frete .money').textContent = toMoney(frete)
    document.querySelector('.box-total .money').textContent = toMoney(frete + discount + total)
}

/* 
  esta funcao monta a estrutura do total
  Ã© bastante parecida com a funcao renderItemTemplate()
  a ideia Ã© a mesma
  definir uma estrutura html
  porem
  ela so recebe um parametro target que Ã© o elemento que vamos adicionar esse html
  o responsavel por mudar os valores Ã© a funcao loadingTotal
*/

function renderTotalContainer(target) {
    let element = "<div class='totais'>"
    element += "<h6> Resumo do Pedido </h6>"
    element += "<div class='box-subtotal'>"
    element += "<span class='text'>SubTotal </span>"
    element += "<span class='money'></span>"
    element += "</div>"
    element += "<div class='box-discounts'>"
    element += "<span class='text'>Descontos </span>"
    element += "<span class='money'></span>"
    element += "</div>"
    element += "<div class='box-frete'>"
    element += "<span class='text'>Frete </span>"
    element += "<span class='money'></span>"
    element += "</div>"
    element += "<div class='box-total'>"
    element += "<span class='text'>Total </span>"
    element += "<span class='money'></span>"
    element += "</div>"
    element += "</div>"


    target.insertAdjacentHTML('beforebegin', element)

}

/* 
  esta funcao deleta os itens do carrinho independente da quantidade
  ele recebe o parametro index que setado na funcao orderItens
  esta funcao adiciona uma classe loading ao carrinho evitando que o usuario interaja com 
  outros elementos do carrinho podendo causar bugs
  o loading so Ã© removido na funcao loadingTotal para evitar que o loading acabe antes dos itens
  serem ordenados

  a variavel element armazena um objeto event que precisamos para acessar o atributo path
  que indica o caminho para cart item e assim podemos usar a funcao remove()
  para remover o item do html
*/
function deleteItem(e, index) {
    cartElement.setAttribute('class', 'carrinho loading');
    let elemento = event
    vtexjs.checkout.getOrderForm()
        .then(function() {
            var itemsToRemove = [{
                "index": index,
                "quantity": 0,
            }]
            return vtexjs.checkout.removeItems(itemsToRemove);
        })

    .done(function(orderForm) {
        elemento.path[2].style.opacity = '0'
        elemento.path[2].remove()
        setTimeout(orderItens(), 500)
        loadingTotal(orderForm)
    });
}

/*
  esta funcao adiciona itens no carrinho
  usamos event.composedPath() para pegar o caminho ate o elemento atual
  Ã© a mesma coisa que event.path[i]
  usamos path para acessar a quantidade do item e muda-la depois que o orderform Ã© atualizado
  dessa forma setamos o loading por item
  o loading Ã© iniciado e terminado na mesma funcao
  uma vez que nao Ã© necessario chamar orderItens para ordenar as funcoes e itens
*/
function addItem(e, index) {
    const element = event.composedPath()
    element[3].classList.add('loading')
    const currentQtd = parseInt(element[3].querySelector('.item-qtd').textContent) + 1
    const qtdElement = element[3].querySelector('.item-qtd')
    const updateItem = { index: index, quantity: currentQtd }

    vtexjs.checkout.updateItems([updateItem], null, false)
        .then(function(orderform) {
            qtdElement.textContent = currentQtd
            element[3].classList.remove('loading')
            loadingTotal(orderform)
        })
}

/*
  esta funcao adiciona itens no carrinho
  usamos event.composedPath() para pegar o caminho ate o elemento atual
  Ã© a mesma coisa que event.path[i]
  usamos path para acessar a quantidade do item e muda-la depois que o orderform Ã© atualizado
  dessa forma setamos o loading por item
  o loading Ã© iniciado e terminado na mesma funcao
  uma vez que nao Ã© necessario chamar orderItens para ordenar as funcoes e itens
*/
function removeItem(e, index) {
    const element = event.composedPath()
    element[3].classList.add('loading')
    const currentQtd = parseInt(element[3].querySelector('.item-qtd').textContent) - 1
    const qtdElement = element[3].querySelector('.item-qtd')
    const updateItem = { index: index, quantity: currentQtd }

    if (currentQtd >= 1) {
        vtexjs.checkout.updateItems([updateItem], null, false)
            .then(function(orderform) {
                qtdElement.textContent = currentQtd
                loadingTotal(orderform)
            })
    }

    element[3].classList.remove('loading')
}

/*
  esta funcao calcula o frete e insere e muda para o valor correto usando loadingTotal

*/
function calcShipping() {
    const cep = document.querySelector('.calc-frete .field-frete').value
    if (!cep) {
        alert('Digite um cep')
    } else {
        vtexjs.checkout.getOrderForm()
            .then(function(orderForm) {
                const postalCode = cep;
                const country = orderForm.storePreferencesData.countryCode;
                const address = {
                    "postalCode": postalCode,
                    "country": country
                };

                return vtexjs.checkout.calculateShipping(address)
            })
            .done(function(orderForm) {
                loadingTotal(orderForm)
                cartElement.setAttribute('class', 'carrinho');
                if (!orderForm.totalizers[1]) { alert('Não pode ser entregue nesse endereço') }

            })

    }
}

/*
  esta funcao carrega as informacoes atuais do carrinho e inicializa o carregamento de itens
  e valores com os metodos:

  renderTotalContainer()
  loadingTotal()

  cada funcao tera sua explicacao acima
*/
vtexjs.checkout.getOrderForm()
    .done(function(orderForm) {
        const cartElement = document.querySelector('.mini-cart-itens')
        const totalElement = document.querySelector('.mini-cart-botao')
        document.querySelector('.mini-cart-qty-admake').textContent = orderForm.items.length
        let item = orderForm.items || [];
        for (item of item) {
            renderItemTemplate(
                cartElement,
                item.name,
                item.imageUrl,
                item.skuName,
                item.productRefId,
                item.additionalInfo.brandName,
                item.quantity,
                item.formattedPrice,
                item.detailUrl
            )
        }

        //carrega html para compra
        renderTotalContainer(totalElement)
            //ordena os items do carrinho para disparar eventos
        orderItens()
            //carrega valores a cada vez q o checkout Ã© mudado
        loadingTotal(orderForm)

});
